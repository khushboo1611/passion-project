use employeedb
select * from employee_det

drop table employee_det

create table employee_det(
emp_id int identity primary key,
emp_name varchar(20),
emp_dept varchar(20),
emp_image varchar(500),
emp_age int,
employee_salary int,
emp_location varchar(20)
)