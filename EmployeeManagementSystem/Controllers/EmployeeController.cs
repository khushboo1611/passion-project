﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeManagementSystem.Models;
namespace EmployeeManagementSystem.Controllers
{
    public class EmployeeController : Controller
    {
        employeedbEntities1 db = new employeedbEntities1();
        //
        // GET: /Employee/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Insert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert(HttpPostedFileBase file, employee_det tb, string emp_name, string dept_name, int emp_age, int employee_salary, string employee_location)
        {
            string image_name=System.IO.Path.GetFileName(file.FileName);
            string path_name=Server.MapPath("~/images/"+image_name);
            file.SaveAs(path_name);

            tb.emp_name = emp_name;
            tb.emp_dept = dept_name;
            tb.emp_image = image_name;
            tb.emp_age = emp_age;
            tb.employee_salary = employee_salary;
            tb.emp_location = employee_location;
            db.employee_det.Add(tb);
            db.SaveChanges();
            ViewBag.mess = "Inserted success";
            return View("Insert");
        }
        public ActionResult ViewRecord()
        {
            return View(db.employee_det.ToList());
        }
        [HttpPost]
        public ActionResult ViewRecord(int []chid)
        {
            foreach (int itemid in chid)
            {
                var findd = db.employee_det.Where(x => x.emp_id == itemid).FirstOrDefault();
                var delete = db.employee_det.Remove(findd);
                
            }
            db.SaveChanges();
            return RedirectToAction("ViewRecord");
        }
        public ActionResult select(int id)
        {
            return View(db.employee_det.Where(x=>x.emp_id==id).ToList());
        }
        [HttpPost]
        public ActionResult select(int idd, string emp_dept, int employee_salary, string employee_location)
        {
            var update = db.employee_det.Where(x => x.emp_id == idd).FirstOrDefault();
            update.emp_dept = emp_dept;
            update.employee_salary = employee_salary;
            update.emp_location = employee_location;
            db.SaveChanges();

            return RedirectToAction("ViewRecord");
        }
	}
}